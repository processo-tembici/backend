"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();
    try {
      await queryInterface.createTable(
        "User",
        {
          _id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true,
          },
          email: {
            type: Sequelize.STRING,
            unique: true,
          },
          name: {
            type: Sequelize.STRING,
          },
        },
        { transaction }
      );

      await queryInterface.createTable(
        "Message",
        {
          _id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true,
          },
          text: {
            type: Sequelize.STRING(1000),
          },
          _id_sender: {
            type: Sequelize.INTEGER,
            references: { model: "User", key: "_id" },
          },
          _id_Group: {
            type: Sequelize.INTEGER,
            references: { model: "Group", key: "_id" },
          },
          date: {
            type: Sequelize.DATE,
            allowNull: false,
          },
        },
        { transaction }
      );

      await queryInterface.createTable(
        "Group",
        {
          _id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true,
          },
          _id_user: {
            type: Sequelize.INTEGER,
            allowNull: false,
          },
          _id_adm: {
            type: Sequelize.INTEGER,
            allowNull: true,
          },
          permission_to_write: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
          },
        },
        { transaction }
      );

      await queryInterface.addConstraint(
        "Message",
        {
          fields: ["_id_sender"],
          type: "foreign key",
          name: "fk_message_id_sender",
          references: {
            //Required field
            table: "User",
            field: "_id",
          },
        },
        { transaction }
      );

      await queryInterface.addConstraint(
        "Message",
        {
          fields: ["_id_Group"],
          type: "foreign key",
          name: "fk_message_id_group",
          references: {
            //Required field
            table: "Group",
            field: "_id",
          },
        },
        { transaction }
      );

      await queryInterface.addConstraint(
        "Group",
        {
          fields: ["_id_user"],
          type: "foreign key",
          name: "fk_group_id_user",
          references: {
            //Required field
            table: "Group",
            field: "_id",
          },
        },
        { transaction }
      );
      await queryInterface.addConstraint(
        "Group",
        {
          fields: ["_id_adm"],
          type: "foreign key",
          name: "fk_group_id_adm",
          references: {
            //Required field
            table: "User",
            field: "_id",
          },
        },
        { transaction }
      );
    } catch (error) {
      return queryInterface.dropTable("Message");
      return queryInterface.dropTable("User");
      return queryInterface.dropTable("Group");
    }
  },

  down: async(queryInterface, Sequelize) => {
  },
};
