module.exports = (sequelize, DataTypes) => {
    const message = sequelize.define('Message', {
      _id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      text: {
        type: DataTypes.STRING,
      },
      _id_sender: {
        type: DataTypes.INTEGER,

    }, {
      tableName: 'Message'
    });
    message.associate = (models) => {
      message.hasMany(models.Message, {
        as: 'message',
        foreignKey: '_id_sender'
      });
      message.hasMany(models.Group, {
        as: 'admin',
        foreignKey: '_id_Adm'
      });
      message.hasMany(models.Group), {
        as: 'group',
        foreignKey: '_id_user'
      }
    }

} 