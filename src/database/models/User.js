module.exports = (sequelize, DataTypes) => {
  const user = sequelize.define('User', {
    _id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    email: {
      type: DataTypes.STRING,
      unique: true
    },
    name: {
      type: DataTypes.STRING,
    }
  }, {
    tableName: 'User'
  });

  user.associate = (models) => {
    user.hasMany(models.Message, {
      as: 'message',
      foreignKey: '_id_sender'
    });
    user.hasMany(models.Group, {
      as: 'admin',
      foreignKey: '_id_Adm'
    });
    user.hasMany(models.Group), {
      as: 'group',
      foreignKey: '_id_user'
    }
  }
} 